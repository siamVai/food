<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'role' => 'ADMIN',
                'uuid' => Str::orderedUuid(),
                'name' => 'Admin',
                'email' => 'admin.food@gmail.com',
                'password' => Hash::make('test12345'),
            ],
            [
                'role' => 'PROVIDER',
                'uuid' => Str::orderedUuid(),
                'name' => 'Provider',
                'email' => 'provider.food@gmail.com',
                'password' => Hash::make('test12345'),
            ]
        ]);
    }
}
