<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('provider_uuid');
            $table->date('date');
            $table->string('channel'); // WEB or EPOS
            $table->string('type');
            $table->string('requester_type');
            $table->string('requester_uuid')->nullable();
            $table->string('status')->default('NEW');
            $table->string('shipping_address_uuid')->nullable();
            $table->timestamp('requested_delivery_timestamp')->nullable();
            $table->double('net_amount')->default(0);
            $table->double('delivery_charge')->default(0);
            $table->string('payment_type')->default('CASH');
            $table->string('payment_uuid')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
