<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('global_parameters', function (Blueprint $table) {
            $table->id();
            $table->string('key_name');
            $table->string('key_value');
            $table->string('creator_uuid');
            $table->timestamps();
            $table->unique('key_name', 'creator_uuid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('global_parameters');
    }
};
