<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', [\App\Http\Controllers\API\Auth\AuthController::class, 'register']);
Route::post('/login', [\App\Http\Controllers\API\Auth\AuthController::class, 'login']);

// Request with provider uuid
Route::group(['middleware' => 'require.provider_uuid'], function () {
    Route::get('/config/property', [\App\Http\Controllers\API\ConfigPropertyController::class, 'get']);
    Route::get('/config/property/{key_name}', [\App\Http\Controllers\API\ConfigPropertyController::class, 'getWithKey']);

    Route::get('/product', [\App\Http\Controllers\API\ProductController::class, 'get']);
    Route::get('/product-option', [\App\Http\Controllers\API\ProductOptionController::class, 'get']);
    Route::get('/product/{product_uuid}', [\App\Http\Controllers\API\ProductController::class, 'show']);
});

Route::post('/order', [\App\Http\Controllers\API\OrderController::class, 'store']);

// Authenticated User
Route::group(['middleware' => 'auth:sanctum'], function () {

    // PROVIDER
    Route::group(['middleware' => 'auth.provider'], function () {

        // Category
        Route::post('/config/property', [\App\Http\Controllers\API\ConfigPropertyController::class, 'store']);

        // Products
        Route::post('/product', [\App\Http\Controllers\API\ProductController::class, 'store']);

        // Product Options
        Route::post('/product-option', [\App\Http\Controllers\API\ProductOptionController::class, 'store']);

        // Product Relation
        Route::post('/product-relation', [\App\Http\Controllers\API\ProductRelationController::class, 'link']);

        // Order
        Route::patch('/order/{id}/status', [\App\Http\Controllers\API\OrderController::class, 'update_status']);

        Route::get('/order-status', [\App\Http\Controllers\API\OrderStatusController::class, 'index']);

        // Global Parameters
        Route::get('/global-parameter', [\App\Http\Controllers\API\GlobalParameterController::class, 'index']);
        Route::post('/global-parameter', [\App\Http\Controllers\API\GlobalParameterController::class, 'store']);
    });

    // All authenticated user can access these apis
    Route::get('/order', [\App\Http\Controllers\API\OrderController::class, 'get']);
    Route::get('/order/{order_id}', [\App\Http\Controllers\API\OrderController::class, 'show']);

});

