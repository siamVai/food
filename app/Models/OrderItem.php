<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    use HasFactory;

    public function option(){
        return $this->hasMany(OrderItemOption::class, 'order_item_id', 'id');
    }

    public function product(){
        return $this->belongsTo(Product::class, 'product_uuid', 'uuid');
    }
}
