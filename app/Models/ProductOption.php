<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductOption extends Model
{
    use HasFactory;

    public function price(){
        return $this->hasOne(ProductOptionPrice::class, 'product_option_uuid', 'uuid');
    }
}
