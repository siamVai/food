<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
    ];

    public function property(){
        return $this->hasMany(ProductProperty::class, 'product_uuid', 'uuid');
    }

    public function price(){
        return $this->hasOne(ProductPrice::class, 'product_uuid', 'uuid');
    }

    public function relation(){
        return $this->hasMany(ProductRelation::class, 'product_uuid', 'uuid');
    }
}
