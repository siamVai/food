<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductRelation extends Model
{
    use HasFactory;

    public function product_option(){
        return $this->belongsTo(ProductOption::class, 'product_option_uuid', 'uuid');
    }
}
