<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductPrice extends Model
{
    use HasFactory;

    protected $hidden = [
        'id',
        'product_uuid',
        'created_at',
        'updated_at',
    ];
}
