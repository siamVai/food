<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    public function item(){
        return $this->hasMany(OrderItem::class, 'order_id', 'id');
    }

    public function requester(){
        return $this->belongsTo(User::class, 'requester_uuid', 'uuid');
    }

    public function guest(){
        return $this->belongsTo(Guest::class, 'requester_uuid', 'uuid');
    }

    public function address(){
        return $this->belongsTo(Address::class, 'shipping_address_uuid', 'uuid');
    }

}
