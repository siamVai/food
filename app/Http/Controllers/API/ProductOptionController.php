<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ProductOption;
use App\Models\ProductOptionPrice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ProductOptionController extends Controller
{
    public function get(Request $request)
    {
        return ProductOption::where('creator_uuid', '=', $request->header('provideruuid'))->with('price')->get();
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "sort_order" => ["required"],
            "name" => ["required"],
        ]);

        $sort_order = $request->sort_order ?? 0;
        $name = $request->name ?? null;
        $description = $request->description ?? null;
        $price = $request->price ?? 0;

        $product_uuid = Str::orderedUuid();
        try {
            DB::beginTransaction();

            $product = new ProductOption();
            $product->uuid = $product_uuid;
            $product->sort_order = $sort_order;
            $product->name = $name;
            $product->visible = 1;
            $product->description = $description;
            $product->creator_uuid = auth()->user()->uuid;
            $product->created_at = now();
            $product->updated_at = now();
            $product->save();

            if($price > 0) {
                $product_price = new ProductOptionPrice();
                $product_price->product_option_uuid = $product_uuid;
                $product_price->price = $price;
                $product_price->created_at = now();
                $product_price ->updated_at = now();
                $product_price->save();
            }

            DB::commit();

            return response()->json('success', 201);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json('error', 400);
        }
    }
}
