<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductOptionPrice;
use App\Models\ProductPrice;
use App\Models\ProductProperty;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function get(Request $request)
    {
        $products = Product::query();
        foreach ($request->all() as $key => $req) {
            if($key === 'category') {
                $products->where('category', '=', $req);
            }
            if($key === 'option' && $req === 'true') {
                $products->with('relation.product_option.price');
            }
        }

        $products = $products->where('creator_uuid', '=', $request->header('provideruuid'))->with('price','property')->get();

        foreach ($products as $product) {
            if ($product->property) {
                $array = null;
                foreach ($product->property as $property) {
                    $array[$property['key_name']] = $property['key_value'];
                }
                $product->properties = $array;
            }
            unset($product['property']);
        }
        return $products;
    }

    public function show(Request $request, $product_uuid)
    {
        $product = Product::where('creator_uuid', '=', $request->header('provideruuid'))->where('uuid', '=', $product_uuid)->with('price','property')->first();
        if (!$product) {
            return response()->json(['message'=>'Not found'], 404);
        }

        $product_options = DB::table('product_relations')->where('product_uuid', $product->uuid)
            ->join('product_options', 'product_relations.product_option_uuid', '=', 'product_options.uuid')
            ->where('product_options.visible', '=', 1)
            ->get();
        $product->options = $product_options;

        if ($product->property) {
            $array = null;
            foreach ($product->property as $property) {
                $array[$property['key_name']] = $property['key_value'];
            }
            $product->properties = $array;
        }
        unset($product['property']);

        return $product;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "sort_order" => ["required"],
            "name" => ["required"],
            "category" => ["required"],
        ]);

        $sort_order = $request->sort_order ?? 0;
        $name = $request->name ?? null;
        $description = $request->description ?? null;
        $category = $request->category ?? null;
        $sub_category = $request->sub_category ?? null;
        $properties = $request->properties ?? [];

        $price = $request->price ?? 0;
        $product_uuid = Str::orderedUuid();

        try {
            DB::beginTransaction();

            $product = new Product();
            $product->uuid = $product_uuid;
            $product->sort_order = $sort_order;
            $product->category = $category;
            $product->sub_category = $sub_category;
            $product->name = $name;
            $product->visible = 1;
            $product->description = $description;
            $product->creator_uuid = auth()->user()->uuid;
            $product->created_at = now();
            $product->updated_at = now();
            $product->save();

            if($price > 0) {
                $product_price = new ProductPrice();
                $product_price->product_uuid = $product_uuid;
                $product_price->price = $price;
                $product_price->created_at = now();
                $product_price ->updated_at = now();
                $product_price->save();
            }

            foreach ($properties as $key => $property) {
                $prop = new ProductProperty();
                $prop->product_uuid = $product->uuid;
                $prop->key_name = $key;
                $prop->key_value = $property;
                $prop->creator_uuid = auth()->user()->uuid;
                $prop->created_at = now();
                $prop->updated_at = now();
                $prop->save();
            }

            DB::commit();

            return response()->json('success', 201);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json('error', 400);
        }
    }
}
