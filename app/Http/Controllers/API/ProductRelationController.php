<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ProductRelation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductRelationController extends Controller
{
    public function link(Request $request)
    {
        $this->validate($request, [
            "product_uuid" => ["required"],
            "product_option_uuid" => ["required", 'array'],
            "group" => ["required"],
        ]);

        try{
            DB::beginTransaction();
            foreach ($request->product_option_uuid as $value) {
                $link = new ProductRelation();
                $link->product_uuid = $request->product_uuid;
                $link->product_option_uuid = $value;
                $link->group = $request->group;
                $link->created_at = now();
                $link->updated_at = now();
                $link->save();
            }
            DB::commit();

            return response()->json('success', 201);
        }catch (\Exception $exception){
            DB::rollBack();
            return response()->json($exception->getMessage(), 400);
        }
    }
}
