<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\GlobalParameter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GlobalParameterController extends Controller
{
    public function index()
    {
        $provider_uuid = auth('sanctum')->user()->uuid ?? null;
        if(!$provider_uuid) return response()->json(["message" => "Not found"], 404);

        try{
            $global_parameters = GlobalParameter::where('creator_uuid',$provider_uuid)->get();

            $arr = null;
            foreach ($global_parameters as $parameter) {
                $arr[$parameter->key_name] = $parameter->key_value;
            }

            return response()->json($arr, 200);
        }catch (\Exception $exception) {
            return response()->json(["message" => $exception->getMessage()], 500);
        }

    }
    public function store(Request $request)
    {
        $provider_uuid = auth('sanctum')->user()->uuid ?? null;
        if(!$provider_uuid) return response()->json(["message" => "Not found"], 404);

        try{
            DB::beginTransaction();
            $global_parameters = new GlobalParameter();
            $global_parameters->key_name = $request->key_name;
            $global_parameters->key_value = $request->key_value;
            $global_parameters->creator_uuid = $provider_uuid;
            $global_parameters->created_at = now();
            $global_parameters->save();
            DB::commit();
            return response()->json(["message" => "success"], 201);
        }catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(["message" => $exception->getMessage()], 500);
        }


    }
}
