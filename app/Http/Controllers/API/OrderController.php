<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Models\Guest;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderItemOption;
use App\Models\OrderStatus;
use App\Models\ProductPrice;
use App\Models\ProductOptionPrice;
use App\Rules\OrderChannel;
use App\Rules\OrderPaymentType;
use App\Rules\OrderType;
use App\Services\Address;
use App\Services\CheckProviderExistence;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class OrderController extends Controller
{
    public function get()
    {
        $provider_uuid = auth('sanctum')->user()->uuid ?? null;
        if (!$provider_uuid) return response()->json(["message" => "Not found"], 404);

        return Order::where('provider_uuid', '=', $provider_uuid)->get();
    }

    public function show($order_id)
    {
        $uuid = auth('sanctum')->user()->uuid ?? null;
        if (!$uuid) return response()->json(["message" => "Unknown Provider"], 404);

        $order = Order::with('item.product', 'item.option.product_option', 'requester', 'guest', 'address')
            ->where('id', $order_id)->first();

        // If the authenticated requester then remove the guest requester property
        // it is to make all requester inside the same property name 'requester'
        if (!$order->requester) {
            unset($order->requester);
            $prop_guest = $order->guest;
            if ($prop_guest) {
                $order->requester = $prop_guest;
                unset($order->guest);
            }
        } else {
            unset($order->guest);
        }

        return $order;

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "provider_uuid" => ["required"],
            "channel" => ["required", "string", new OrderChannel],
            "type" => ["required", "string", new OrderType],
            "requested_delivery_timestamp" => ["required"],
            "payment_type" => ["required", "string", new OrderPaymentType],
            "items" => ["required", "array"],
        ]);

        $provider_uuid = $request->provider_uuid;
        $date = $request->date ?? now();
        $channel = $request->channel;
        $type = $request->type;
        $requested_delivery_timestamp = $request->requested_delivery_timestamp;
        $payment_type = $request->payment_type;
        $items = $request->items;

        $shipping_address = $request->shipping_address ?? null;

        // Check provider existence
        if (!CheckProviderExistence::check($provider_uuid)) {
            return response()->json(["message" => 'Unknown Provider'], 404);
        }

        try {
            DB::beginTransaction();;

            if (auth('sanctum')->check() && auth('sanctum')->user()->role === 'PROVIDER') {
                // Created by provider: LOCAL order
                $requester_type = 'PROVIDER';
            } else {

                // Created by guest or auth user
                if (!auth('sanctum')->check()) {

                    $requester_type = 'GUEST';

                    $name = $request->requester['name'] ?? null;
                    $phone = $request->requester['phone'] ?? null;
                    $email = $request->requester['email'] ?? '';

                    if (!$name || !$phone) {
                        return response()->json(["message" => 'Guest information not found'], 404);
                    }

                    $guest = new Guest();
                    $guest->uuid = Str::orderedUuid();
                    $guest->name = $name;
                    $guest->phone = $phone;
                    $guest->email = $email;
                    $guest->created_at = now();
                    $guest->updated_at = now();
                    $guest->save();

                    $requester_uuid = $guest->uuid;

                    //Address Information
                    if ($shipping_address) {
                        $address_id = Address::store(new Request([
                            "creator_uuid" => $requester_uuid,
                            "postcode" => $shipping_address['postcode'],
                            "city" => $shipping_address['town'],
                            "area" => $shipping_address['area'],
                            "road" => $shipping_address['road'],
                            "house" => $shipping_address['house'],
                        ]));
                        if (!$address_id) return response()->json(["message" => $address_id], 500);
                    }

                    $shipping_address_uuid = $address_id ?? null;
                } else {
                    $requester_type = 'CONSUMER';
                    $requester_uuid = auth('sanctum')->user()->uuid;
                    $shipping_address_uuid = null;
                }
            }

            $order = new Order();
            $order->provider_uuid = $provider_uuid;
            $order->date = $date;
            $order->channel = $channel;
            $order->type = $type;
            $order->requester_type = $requester_type;
            $order->requester_uuid = $requester_uuid ?? null;
            $order->shipping_address_uuid = $shipping_address_uuid ?? null;
            $order->requested_delivery_timestamp = $requested_delivery_timestamp;
            $order->status = 'NEW';
            $order->net_amount = 0;
            $order->delivery_charge = 0;
            $order->payment_type = $payment_type;
            $order->payment_uuid = null;
            $order->created_at = now();
            $order->updated_at = now();
            $order->save();

            $total_product_price = 0;

            foreach ($items as $product) {

                $product_uuid = $product['product_uuid'] ?? null;
                $qty = $product['unit'] ?? 1;
                if (!$product_uuid) return response()->json(["message" => "Unknown product"], 404);

                $product_price = ProductPrice::where('product_uuid', $product_uuid)->first();
                if ($product_price) {
                    $p_price = $product_price->price;
                } else {
                    $p_price = 0;
                }

                $order_item = new OrderItem();
                $order_item->order_id = $order->id;
                $order_item->product_uuid = $product['product_uuid'] ?? '';
                $order_item->unit = $qty;
                $order_item->net_amount = $p_price * $qty;
                $order_item->comment = $product->comment ?? null;
                $order_item->created_at = now();
                $order_item->save();

                $total_product_option_price = 0;

                if (isset($product['options'])) {
                    foreach ($product['options'] as $option) {
                        $product_option_uuid = $option['product_option_uuid'] ?? null;
                        $unit = $product['unit'] ?? 1;
                        if (!$product_option_uuid) return response()->json(["message" => "Unknown product option"], 404);

                        $product_option_price = ProductOptionPrice::where('product_option_uuid', $product_option_uuid)->first();
                        if ($product_option_price) {
                            $po_price = $product_option_price->price;
                        } else {
                            $po_price = 0;
                        }
                        $order_item_option = new OrderItemOption();
                        $order_item_option->order_item_id = $order_item->id ?? 0;
                        $order_item_option->product_option_id = $product_option_uuid;
                        $order_item_option->unit = $unit;
                        $order_item_option->net_amount = $po_price * $unit;
                        $order_item_option->created_at = now();
                        $order_item_option->save();

                        $total_product_option_price += $po_price * $unit;
                    }
                }

                // Update order item price
                $order_item->net_amount += $total_product_option_price;
                $order_item->save();

                $total_product_price += $order_item->net_amount;

            }

            // update order price
            $order->net_amount = $total_product_price;
            $order->save();

            DB::commit();

            return response()->json(["message" => 'Order created'], 201);

        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json($exception->getMessage(), 400);
        }
    }

    public function update_status(Request $request, $id)
    {
        $this->validate($request, [
            "status" => ["required"]
        ]);

        $status = OrderStatus::where('name', $request->status)->first();
        if (!$status) {
            return response()->json(["message" => 'Status not found'], 404);
        }

        try {
            DB::beginTransaction();
            $order = Order::findOrFail($id);
            $order->status = $request->status;
            $order->save();
            DB::commit();
            return response()->json(["message" => 'Status updated'], 202);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(["message" => 'Status cannot be updated'], 400);
        }
    }
}
