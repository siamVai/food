<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ConfigProperty;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ConfigPropertyController extends Controller
{
    public function get(Request $request)
    {
        return ConfigProperty::where('creator_uuid', '=', $request->header('provideruuid'))->get();
    }

    public function getWithKey(Request $request, $key_name)
    {
        return ConfigProperty::with('child')->where('creator_uuid', '=', $request->header('provideruuid'))->where('key_name', $key_name)->get();
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "platform" => ["required"],
            "sort_order" => ["required"],
            "key_name" => ["required"],
            "name" => ["required"],
        ]);

        $platform = $request->platform; // WEB, EPOS
        $sort_order = $request->sort_order;
        $key_name = $request->key_name;
        $name = $request->name;
        $description = $request->description ?? null;

        $parent_id = $request->parent_id ?? null;

        try{
            DB::beginTransaction();

            $conf = new ConfigProperty();
            $conf->platform = $platform;
            $conf->parent_id = $parent_id;
            $conf->sort_order = $sort_order;
            $conf->key_name = $key_name;
            $conf->value = $name;
            $conf->name = $name;
            $conf->description = $description;
            $conf->creator_uuid = auth()->user()->uuid;
            $conf->created_at = now();
            $conf->updated_at = now();
            $conf->save();

            DB::commit();

            return response()->json('success', 201);
        }catch (\Exception $exception) {
            DB::rollBack();
            return response()->json('error', 400);
        }
    }
}
