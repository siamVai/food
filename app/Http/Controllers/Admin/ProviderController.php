<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserProperty;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ProviderController extends Controller
{
    public function index()
    {
        return view('provider');
    }

    public function get()
    {
        return User::where('role', 'PROVIDER')->with('property')->get();
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        ]);

        $name = $request->name;
        $email = $request->email;
        $password = $request->password ?? 'Test12345';

        $user = User::create([
            'role' => 'PROVIDER',
            'uuid' => Str::orderedUuid(),
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password),
        ]);

        $array = [
            // 'start_date' => $start_date,
        ];

        foreach ($array as $key => $value) {
            UserProperty::create([
                'user_id' => $user->id,
                'key_name' => $key,
                'key_value' => $value,
            ]);
        }

        return response()->json('success', 201);
    }
}
