<?php

namespace App\Services;

use App\Models\User;

class CheckProviderExistence
{
    public static function check($provider_uuid)
    {
        $provider = User::where('role', 'PROVIDER')->where('uuid', $provider_uuid)->first();
        if(!$provider) {
            return false;
        }
        return true;
    }
}
