<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Address
{
    public static function store(Request $request)
    {
        try{
            DB::beginTransaction();
            $uuid = Str::orderedUuid();

            $address = new \App\Models\Address();

            $address->uuid = $uuid;
            $address->creator_type = 'GUEST';
            $address->creator_uuid = $request->creator_uuid;
            $address->postcode = $request->postcode;
            $address->city = $request->town;
            $address->area = $request->area;
            $address->road = $request->road;
            $address->house = $request->house;
            $address->save();
            DB::commit();

            return $uuid;
        }catch (\Exception $exception){
            DB::rollBack();
            return $exception->getMessage();
        }
    }
}
