@extends('layouts.app')
@section('title') Provider @endsection

@section('content')
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">Providers</h4>
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboards</a></li>
                                <li class="breadcrumb-item active">Providers</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-4">Add Providers</h4>
                            <form method="POST" enctype="multipart/form-data" @submit.prevent="store()">
                                @csrf

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="mb-3">
                                            <label for="name" class="form-label"> Name</label>
                                            <input type="text" class="form-control" id="name" name="name" v-model="name" placeholder="New Provider" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="mb-3">
                                            <label for="email" class="form-label">Email</label>
                                            <input type="text" class="form-control" id="email" name="email" v-model="email" placeholder="email@email.com" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="password" class="form-label">Password</label>
                                            <input type="password" class="form-control" id="password" name="password" v-model="password" placeholder="******">
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <button type="submit" class="btn btn-primary w-md" :class="loading ? 'disabled': ''">@{{ loading ? 'Saving...' : 'Save' }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-8 col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Provider List</h4>

                            <div class="table-responsive">
                                <table class="table table-sm m-0">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="(val, index) in providers">
                                        <th scope="row">@{{ index + 1 }}</th>
                                        <td>
                                            @{{ val.name }} <br>
                                        </td>
                                        <td>
                                            UUID: <b>@{{ val.uuid }}</b> <br>
                                        </td>
                                        <td>
                                            <a class="btn btn-outline-secondary btn-sm edit" title="Edit">
                                                <i class="fas fa-pencil-alt" title="Edit"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        var app = new Vue({
            el: '#app',
            data: {
                providers: [],
                email: '',
                name: '',

                loading: false,
            },
            created() {
                var self = this;
                self.get_providers();
            },
            methods: {
                get_providers: function () {
                    var self = this;
                    axios.get('/provider/get').then(response => {
                        self.providers = response.data;
                    })
                },
                store: function (){
                    var self = this;
                    self.loading = true;
                    if (!confirm('Are You Sure?')){
                        return false;
                    }
                    axios.post('/provider', {
                        email: self.email,
                        name: self.name,
                        password: self.password,
                    })
                        .then(response => {
                            self.clear();
                            self.get_providers();
                            self.loading = false;
                        })
                        .catch(error => {
                            console.log(error);
                            self.loading = false;
                        })
                },
                clear: function () {
                    var self = this;
                    self.name = '';
                    self.start_date = '';
                }
            }
        })
    </script>
@endpush
